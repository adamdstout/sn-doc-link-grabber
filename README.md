# SN Doc Link Grabber Browser Extension
Quickly create version insensitive links for ServiceNow documentation.  This is extremely useful to paste the links into external documentation to ensure that they will not become outdated with the next release of ServiceNow.

This extension is no way affiliated with ServiceNow.  This extension is not endorsed or supported by ServiceNow.

## Installation
"ServiceNow Doc Link Grabber" is available through the [chrome web store](https://chrome.google.com/webstore/detail/).

You can also install this extension on by downloading the source from GitHub and loading the unpacked extension manually.

## Contributing
Allowed to contribute, not to republish the extension to the Chrome / FireFox / Any other Store or marketplace.
Also not allowed to use parts of the extension functionality or republish to other stores / markets as part of other services, without prior consent of the author: Adam Stout.

https://gitlab.com/adamdstout/sn-doc-link-grabber

If you notice any errors, please create a new issue on GitLab.

# Attribution
This extension uses the feathericons `link` icon, licensed under the MIT license, viewable at https://github.com/feathericons/feather/blob/master/LICENSE

This extension was based on work done in the [Quick Copy URL Browser Extension](https://github.com/vantezzen/quick-copy-url)

# License
SN Doc Link Grabber is licensed under the MIT License.
/**
 * 
 * SN Doc Link Grabber
 * 
 * @copyright   Copyright Adam Stout (https://adamstout.dev/)
 * @link        https://gitlab.com/adamdstout/sn-doc-link-grabber
 * @license     https://opensource.org/licenses/mit-license.php MIT License
 * 
 */

const EXT_FULL_NAME = 'ServiceNow Doc Link Grabber';

function whichSite (url) {
  console.log('[' + EXT_FULL_NAME + '] url: ', url);
  var matches = url.match(/^https?:\/\/([^\.]*)\.servicenow\.com\//);
  if(matches.length == 2)
  {
    return matches[1];
  } else {
    console.log('[' + EXT_FULL_NAME + '] Could not determine which site we are on: ', err);
  }
}

const SN_FORMATTED_LINK = 'https://docs.servicenow.com/csh?version=latest&topicname=';
const copyURL = (sendResponse) => {

  var fullURL = window.location.href;

  var site = whichSite(fullURL);

  console.log('[' + EXT_FULL_NAME + '] Site: ' + site);

  switch(site)
  {
    case "developer":
      var matches = fullURL.match(/\.com(?:.*)\/(?:(?:guides)|(?:reference\/(?:[^\/]+))|(?:learn\/(?:[^\/]+)))\/([^\/\#\n]+)(?:.*)?$/);
      if(matches.length == 2)
      {
        var newURL = fullURL.replace('/' + matches[1] + '/', '/latest/');
        navigator.clipboard.writeText(newURL)
          .then(() => {
            sendResponse('success');
            console.log('[' + EXT_FULL_NAME + '] Text copied to clipboard');
          })
          .catch(err => {
            sendResponse('failed');
            console.log('[' + EXT_FULL_NAME + '] Could not copy text: ', err);
          });
      }
      break;
    case "docs":
    default:
      var matches = fullURL.match(/([^\/][-\d\w\.]+)\.html(?:#.*)?$/);
      if(matches.length == 2)
      {
        var docURL = matches[1];
        navigator.clipboard.writeText(SN_FORMATTED_LINK + docURL)
        .then(() => {
          sendResponse('success');
          console.log('[' + EXT_FULL_NAME + '] Text copied to clipboard');
        })
        .catch(err => {
          sendResponse('failed');
          console.log('[' + EXT_FULL_NAME + '] Could not copy text: ', err);
        });
      }
      break;
  }
  

  
}

chrome.runtime.onMessage.addListener((type, s, sendResponse) => {
  if (type === "copy") {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
      // Firefox doesn't have clipboard-write permission - directly try to copy
      copyURL(sendResponse);
    } else {
      // Get clipboard write permissions
      navigator.permissions.query({name: "clipboard-write"}).then(result => {
        if (result.state == "granted" || result.state == "prompt") {
          copyURL(sendResponse);
        } else {
          sendResponse('failed');
          // User has not granted access to the clipboard - abort
          console.log('[' + EXT_FULL_NAME + '] Cannot use clipboard');
        }
      });
    }
    return true;
  }
})
# Privacy Policy
This page is used to inform visitors regarding the policies with the collection, use, and disclosure of Personal Information if anyone decided to use SN Doc Link Grabber.

If you choose to use my SN Utils, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.


## Disclosure for requested permissions
Manifest Permissions:
- clipboardWrite: Push the modified URL to your clip board


## Changes to This Privacy Policy
This Privacy Policy my be updated from time to time. Thus, you are advised to review this page periodically for any changes. Changes be post on this page. These changes are effective immediately after they are posted on this page.

## Contact Us
If you have any questions or suggestions about my Privacy Policy, do not hesitate to open an issue in gitlab.

